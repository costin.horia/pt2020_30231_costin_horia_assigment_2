package ro.tuc.pt.assigment2;

public class Task {
	
	private int idtask;
	private int arrivalTime;
	private int processingTime;
	
	public Task(int idtask, int arrivalTime, int processingTime) {
		super();
		this.idtask = idtask;
		this.arrivalTime = arrivalTime;
		this.processingTime = processingTime;
	}
	
	
	
	public Task() {
		super();
		// TODO Auto-generated constructor stub
	}



	public int getIdtask() {
		return idtask;
	}
	public void setIdtask(int idtask) {
		this.idtask = idtask;
	}
	public int getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getProcessingTime() {
		return processingTime;
	}
	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}
}
