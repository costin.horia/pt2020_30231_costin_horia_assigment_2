package ro.tuc.pt.assigment2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Manager implements Runnable{
	
	public InputsRecover inputs;
	public int nr_cozi ;
	public int nr_clienti ;
	public int timpul_limita;
	public int min_arrival_time;
	public int max_arrival_time;
	public int min_service_time;
	public int max_service_time;
	private List<Task> generatedTasks;
	public Scheduler scheduler;
	public StringBuilder data_out;
	
	public double average_waiting_time = 0;
	
		private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	
		public void generateNRandomTasks() {
		int sum = 0;
		for(int i=1; i<=this.nr_clienti ;i++)
		{
			Task t=new Task( i, getRandomNumberInRange( this.min_arrival_time ,this.max_arrival_time), getRandomNumberInRange(this.min_service_time ,this.max_service_time));
			this.generatedTasks.add(t);
			//System.out.println( "("+t.getIdtask()+","+t.getArrivalTime()+","+t.getProcessingTime()+")");
			sum +=t.getProcessingTime();
		}
		this.average_waiting_time =(double) sum /this.nr_clienti;
		this.generatedTasks.sort(new SortbyArrivalTime());
	}
		
		public void average_time() {
			System.out.println("Average waiting time: "+this.average_waiting_time);
			data_out.append("Average waiting time: "+this.average_waiting_time);
		}


	public Manager(String filename) {
		super();
		generatedTasks= new ArrayList<Task>();
		this.inputs = new InputsRecover(filename);
		inputs.ReadFromFile();
		this.nr_cozi = inputs.getNumberOfQueues();
		this.nr_clienti = inputs.getNumberOfClients();
		this.timpul_limita = inputs.getSimulationInterval();
		this.min_arrival_time = inputs.getMinArrivalTime();
		this.max_arrival_time = inputs.getMaxArrivalTime();
		this.min_service_time = inputs.getMinServiceTime();
		this.max_service_time = inputs.getMaxServiceTime();
		this.generateNRandomTasks();
		this.scheduler= new Scheduler(this.nr_cozi);
		this.data_out = new StringBuilder();
		
	}
	
	public void WriteinFile(String file, String toWrite) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.append(toWrite);
			writer.append("\n");
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
		/*for(int i=1;i<=this.nr_clienti;i++)
			{
			Random r=new Random();
			Task t=new Task(i, getRandomNumberInRange(this.min_arrival_time, this.max_arrival_time), getRandomNumberInRange(this.min_service_time,this.max_service_time) );
			this.clients.add(t);
			System.out.println( "("+t.getIdtask()+","+t.getArrivalTime()+","+t.getProcessingTime()+")");
			}*/
			@Override
			public void run() {
				// TODO Auto-generated method stub
				int currentTime=0;
				
				while(currentTime<=timpul_limita)
				{
				System.out.println("Time "+currentTime);
				System.out.print("Waiting clients:");
				data_out.append("Time "+currentTime+"\n");
				data_out.append("Waiting clients:");
				//for(Task e:generatedTasks )
				//{
						//System.out.print(" ("+e.getIdtask()+","+e.getArrivalTime()+","+e.getProcessingTime()+");");
				//}	
					//System.out.println("");
				
					Iterator<Task> itr = generatedTasks.iterator(); 
			        while (itr.hasNext()) 
			        { 
			            Task x = (Task)itr.next();  
			            //System.out.print(" ("+x.getIdtask()+","+x.getArrivalTime()+","+x.getProcessingTime()+");");
			            if(x.getArrivalTime()==currentTime)
						{
			            	Task found= new Task();
			            	//System.out.println("");
			            	found.setIdtask(x.getIdtask());
			            	found.setArrivalTime(x.getArrivalTime());
			            	found.setProcessingTime(x.getProcessingTime());
							//System.out.println( "->("+x.getIdtask()+","+x.getArrivalTime()+","+x.getProcessingTime()+")");
							this.scheduler.dispatch(found);
							itr.remove();
						} 
			            //System.out.print(" ("+x.getIdtask()+","+x.getArrivalTime()+","+x.getProcessingTime()+");");
			        }
			     //if()
			        //scheduler.ShowServers();
			        //System.out.println("");
			        Iterator<Task> itr2 = generatedTasks.iterator(); 
			        while (itr2.hasNext()) 
			        { 
			        	Task x = (Task)itr2.next();
			            System.out.print(" ("+x.getIdtask()+","+x.getArrivalTime()+","+x.getProcessingTime()+");");
			            data_out.append(" ("+x.getIdtask()+","+x.getArrivalTime()+","+x.getProcessingTime()+");");
			        }
			        //System.out.println( "->("+found.getIdtask()+","+found.getArrivalTime()+","+found.getProcessingTime()+")");		
					System.out.println("");
					data_out.append("\n");
					data_out.append(this.scheduler.showQueues(this.nr_cozi));
					data_out.append("\n");
					data_out.append("\n");
					data_out.append("\n");
					for(Server e:this.scheduler.getServers() ) {
						e.remove_and_decrement();
					}
					
					System.out.println("");
					System.out.println("");
					
					//if(currentTime==)
					currentTime++;
				}
				
			}
		
}
