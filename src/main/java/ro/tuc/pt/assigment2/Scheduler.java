package ro.tuc.pt.assigment2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Scheduler {

	private ArrayList<Server> servers;
	private int maxNrServers;
	//private int maxTasksPerSAerver=5;
	
	public Scheduler(int maxNrServers) {
		this.servers = new ArrayList<Server>();
		this.maxNrServers = maxNrServers;
		for(int i=1;i<=maxNrServers;i++)
		{
			Server newServer= new Server(i);
			newServer.setWaitingPeriod(new AtomicInteger(0));
			servers.add(newServer);
		}
	}
	
	public String showQueues(int nrQueues)
	{
		StringBuilder data = new StringBuilder();
		for(Server e:this.getServers())
		{
			System.out.println("");
			data.append("\n");
			System.out.print("Queue "+e.getIdServer()+/*" cu timpul  de asteptare "+e.getWaitingPeriod()+*/" : ");
			data.append("Queue "+e.getIdServer()+/*" cu timpul  de asteptare "+e.getWaitingPeriod()+*/" : ");
			data.append(e.ServerContents());
			
		}
		String data_final = String.valueOf(data);
		
		return data_final;
	}
	
	public void ShowServers()
	{
		Iterator<Server> i = servers.listIterator(); 
		while(i.hasNext())
		{
			System.out.println("");
			System.out.print("Queue "+i.next().getIdServer()+" :");
			i.next().ServerContents();
			
			//Thread newT = new Thread(i.next());
			//newT.run();
		}
	}

	public List<Server> getServers() {
		return servers;
	}

	public void setServers(ArrayList<Server> servers) {
		this.servers = servers;
	}

	public int getMaxNrServers() {
		return maxNrServers;
	}

	public void setMaxNrServers(int maxNrServers) {
		this.maxNrServers = maxNrServers;
	}
	
	/*public void () {
		for(Server e:servers) {
			if(e.getTasks().isEmpty() == false) {
				for(Task ee: e.getTasks()) {
					int new_processing_time = ee.getProcessingTime() - 1;
					 ee.setProcessingTime(new_processing_time);
				}
			}
		}
	}*/
	
	public Server min_Server() {
		Server min=null;
		int minTime=100000;
		for(Server e: this.getServers())
		{
			if(e.getTasks().isEmpty()==true)
			{
				min=e;
				break;
			}
			//System.out.println("ARATA WAITING PERIODUL: "+e.getWaitingPeriod().intValue());
			if(e.getWaitingPeriod().intValue() < minTime)
			{
				minTime = e.getWaitingPeriod().intValue();
				min=e;
			}
		}
		//System.out.println("Timp:"+minTime);
		return min;
	}
	
	public void dispatch(Task t){
		Server min = min_Server();
		try {
			min.addTask(t);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(min.getTasks().size()==0)
		{
			Thread newT = new Thread(min);
			newT.start();
		}
	}
}
