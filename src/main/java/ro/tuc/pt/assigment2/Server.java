package ro.tuc.pt.assigment2;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
	private int idServer;
	private BlockingQueue<Task> tasks ;
	private AtomicInteger waitingPeriod; //= new AtomicInteger(0);
	
	public void addTask(Task newTask) throws InterruptedException {
		//System.out.println("added task:("+newTask.getIdtask()+ "," + newTask.getArrivalTime() + ","+ newTask.getProcessingTime() +")");
		this.getTasks().add(newTask);
		//this.showServer();
		this.getWaitingPeriod().addAndGet(newTask.getProcessingTime());
	}
	
	
	public Server(int idServer) {
		super();
		tasks =new LinkedBlockingDeque<Task>(10);
		this.idServer = idServer;
	}


	/*public void removetask() {
		try {
			this.tasks.take();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	public void remove_and_decrement () {
		if(this.getTasks().isEmpty()==false) {
			for(Task e: this.getTasks()) {
				if(e.getProcessingTime() == 1)
				{
					//this.getTasks().
					try {
						this.getTasks().take();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					//this.setWaitingPeriod(waitingPeriod);
					//System.out.println("TASK-UL TREBUIE SCOS");
				}else {
				int new_processing_time = e.getProcessingTime() - 1;
				 e.setProcessingTime(new_processing_time);
				}
			}
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
			//System.out.println("closed");
			//this.getTasks().remove();
			System.out.println("dadadadadadadadaad");
			Task head=null;
			try {
				head = this.getTasks().take();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("Head: "+head.toString());
				try {
					
					Thread.sleep(head.getProcessingTime());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			this.waitingPeriod.addAndGet(-head.getProcessingTime());
		
		
		
	}

	public BlockingQueue<Task> getTasks() {
		return tasks;
	}

	public void setTasks(BlockingQueue<Task> tasks) {
		this.tasks = tasks;
	}

	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}
	
	public int getIdServer() {
		return idServer;
	}


	public void setIdServer(int idServer) {
		this.idServer = idServer;
	}


	public String ServerContents() {
		StringBuilder data = new StringBuilder();
		if(this.getTasks().isEmpty()==true)
		{
			System.out.print("closed");
			data.append("closed");
		}
		else for(Task e:this.getTasks())
		{
			System.out.print("("+e.getIdtask()+","+e.getArrivalTime()+","+e.getProcessingTime()+"), ");
			data.append("("+e.getIdtask()+","+e.getArrivalTime()+","+e.getProcessingTime()+"), ");
		}
		
		String data_final = String.valueOf(data);
		
		return data_final;
	}

}
