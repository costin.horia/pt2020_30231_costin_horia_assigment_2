package ro.tuc.pt.assigment2;

import java.nio.file.Files;
import java.nio.file.Paths;

public class InputsRecover {

	private int NumberOfClients;
	private int NumberOfQueues;
	private int SimulationInterval;
	private int MinArrivalTime;
	private int MaxArrivalTime;
	private int MinServiceTime;
	private int MaxServiceTime;
	private String filename;
	
	
	public InputsRecover(String filename) {
		super();
		this.filename = filename;
		// TODO Auto-generated constructor stub
	}

	public int getNumberOfClients() {
		return NumberOfClients;
	}

	public int getNumberOfQueues() {
		return NumberOfQueues;
	}

	public int getSimulationInterval() {
		return SimulationInterval;
	}

	public int getMinArrivalTime() {
		return MinArrivalTime;
	}

	public int getMaxArrivalTime() {
		return MaxArrivalTime;
	}

	public int getMinServiceTime() {
		return MinServiceTime;
	}

	public int getMaxServiceTime() {
		return MaxServiceTime;
	}

	public void setNumberOfClients(int numberOfClients) {
		NumberOfClients = numberOfClients;
	}

	public void setNumberOfQueues(int numberOfQueues) {
		NumberOfQueues = numberOfQueues;
	}

	public void setSimulationInterval(int simulationInterval) {
		SimulationInterval = simulationInterval;
	}

	public void setMinArrivalTime(int minArrivalTime) {
		MinArrivalTime = minArrivalTime;
	}

	public void setMaxArrivalTime(int maxArrivalTime) {
		MaxArrivalTime = maxArrivalTime;
	}

	public void setMinServiceTime(int minServiceTime) {
		MinServiceTime = minServiceTime;
	}

	public void setMaxServiceTime(int maxServiceTime) {
		MaxServiceTime = maxServiceTime;
	}

		public static String readFileAsString(String fileName)throws Exception 
	  { 
	    String data = ""; 
	    data = new String(Files.readAllBytes(Paths.get(fileName))); 
	    return data; 
	  } 
	
	public void ReadFromFile() {
	//int v[]= {0};	
	String data = null;
	//int j=0;
	
	try {
		data = readFileAsString(filename);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	    //System.out.println(data); 
	    String[] s=data.split("\n");
	  //System.out.println("->"+Arrays.toString(s));
	    
	  for(int i=0;i<s.length;i++)
	  {
		  if(i==0)
		  {
			 // int NumberOfClients1 = Integer.parseInt(s[i]);
			  //
			  String ss = s[i].replace("\n", "").replace("\r", "");
			  int NumberOfClients1 = Integer.parseInt(ss);
			  //System.out.println("->>"+NumberOfClients1+"__}}");
			  setNumberOfClients(NumberOfClients1);
			  //v[j] = Integer.parseInt(s[i]);
		  }
		  
		  if(i==1)
		  {
			  String ss = s[i].replace("\n", "").replace("\r", "");
			  int NumberOfClients1 = Integer.parseInt(ss);
			  //System.out.println("->>"+NumberOfClients1+"__}}");
			  setNumberOfQueues(NumberOfClients1);
		  }
		  
		  if(i==2)
		  {
			  String ss = s[i].replace("\n", "").replace("\r", "");
			  int NumberOfClients1 = Integer.parseInt(ss);
			  //System.out.println("->>"+NumberOfClients1+"__}}");
			  setSimulationInterval(NumberOfClients1);

		  }
		  
		  if(i==s.length-2)
		  {
			  String[] s_final=s[i].split(",");
			  //System.out.println("->>"+s_final[0]+"->>"+s_final[1]);
			  
			  String ss1 = s_final[0].replace("\n", "").replace("\r", "");
			  
			  String ss2 = s_final[1].replace("\n", "").replace("\r", "");
			  
			  setMinArrivalTime(Integer.parseInt(ss1));
			  
			  setMaxArrivalTime(Integer.parseInt(ss2));
			  //this.MinArrivalTime = Integer.parseInt(s_final[0]);
			  //v[j]=Integer.parseInt(s_final[0]);
			 
			  //this.MaxArrivalTime = Integer.parseInt(s_final[1]);
			  //v[j]=Integer.parseInt(s_final[1]);
		  }
		  
		if(i==s.length-1)
		  {
			  String[] s_final=s[i].split(",");
			  //System.out.println("->>"+s_final[0]+"->>"+s_final[1]);
			  
			  String ss1 = s_final[0].replace("\n", "").replace("\r", "");
			  
			  String ss2 = s_final[1].replace("\n", "").replace("\r", "");
			  
			  setMinServiceTime(Integer.parseInt(ss1));
			  
			  setMaxServiceTime(Integer.parseInt(ss2));
			  //this.MinServiceTime = Integer.parseInt(s_final[0]);
			  //v[j]=Integer.parseInt(s_final[0]);
			  
			  //this.MaxServiceTime = Integer.parseInt(s_final[1]);
			  //v[j]=Integer.parseInt(s_final[1]);
		  }
		
	  }
	}
}
